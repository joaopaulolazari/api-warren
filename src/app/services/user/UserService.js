const identifierGenerator = require('src/infra/support/identifierGenerator');

module.exports = ({ userRepository, logger }) => ({

    create: async (date) => {
        try {
            date.user_id = identifierGenerator.newUUID();
            return await userRepository.create(date);
        } catch (error) {
            logger.error(error);
            throw error;
        }
    },

    getUserById: async (id) => {
        try {
            return await userRepository.getUser(id);
        } catch (error) {
            logger.error(error);
            throw error;
        }
    },

    update: async (id, data) => {
        try {
            return await userRepository.updateUser(id, data);
        } catch (error) {
            logger.error(error);
            throw error;
        }
    },

    delete: async (id) => {
        try {
            return await userRepository.deleteUser(id);
        } catch (error) {
            logger.error(error);
            throw error;
        }
    }
});
