module.exports = ({ config }) => ({

    getConfig: (path = '') => {
        let currentConfig = { ...config };

        path.split('.').forEach(key => {
            currentConfig = currentConfig || {};
            currentConfig = currentConfig[key];

            if (currentConfig === undefined)
                currentConfig = null;
        });

        return currentConfig;
    }

});
