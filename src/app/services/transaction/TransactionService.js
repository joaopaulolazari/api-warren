const identifierGenerator = require('src/infra/support/identifierGenerator');
const TransactionType = require('src/domain/enum/TransactionType');

module.exports = ({ transactionRepository, logger }) => ({
    create: async (date) => {
        try {
            date.transaction_id = identifierGenerator.newUUID();
            return await transactionRepository.create(date);
        } catch (error) {
            logger.error(error);
            throw error;
        }
    },
    getTransactionById: async (id) => {
        try {
            return await transactionRepository.getTransaction(id);
        } catch (error) {
            logger.error(error);
            throw error;
        }
    },
    update: async (id, data) => {
        try {
            return await transactionRepository.updateTransaction(id, data);
        } catch (error) {
            logger.error(error);
            throw error;
        }
    },
    delete: async (id) => {
        try {
            return await transactionRepository.deleteTransaction(id);
        } catch (error) {
            logger.error(error);
            throw error;
        }
    },
    getBalanceByUserId: async (id) => {
        try {
            const transactions = await transactionRepository.getByUserId(id);
            return transactions
                .reduce((acc, val) => {
                    if (TransactionType.DEPOSIT === val.type) return acc + val.value;
                    return acc - val.value;
                }, 0);
        } catch (error) {
            logger.error(error);
            throw error;
        }
    },
    getTransactionsByUserId: async (id) => {
        try {
            return await transactionRepository.getByUserId(id);
        } catch (error) {
            logger.error(error);
            throw error;
        }
    }
});
