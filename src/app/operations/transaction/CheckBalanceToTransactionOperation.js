const BusinessException = require('src/infra/exception/BusinessException');
const EnumMessage = require('src/domain/enum/EnumMessage');

module.exports = ({ transactionService }) => ({
    execute: async (data) => {
        const { value, user_id: userId } = data;
        const balance = await transactionService.getBalanceByUserId(userId);
        if (balance < value) throw new BusinessException(EnumMessage.INSUFFICIENT_BALANCE);
        return balance;
    }
});
