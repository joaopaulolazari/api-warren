const TransactionType = require('src/domain/enum/TransactionType');

module.exports = ({ transactionService, userService, checkBalanceToTransactionOperation }) => ({
    execute: async (data) => {
        const { user_id: userId, type } = data;
        await userService.getUserById(userId);

        if ([TransactionType.DISCHARGE, TransactionType.PAYMENT].includes(type))
            await checkBalanceToTransactionOperation.execute(data);

        return transactionService.create(data);
    }
});
