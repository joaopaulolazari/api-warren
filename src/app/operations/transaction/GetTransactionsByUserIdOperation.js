module.exports = ({ transactionService }) => ({
    execute: async (id) => await transactionService.getTransactionsByUserId(id)
});
