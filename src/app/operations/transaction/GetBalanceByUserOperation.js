module.exports = ({ transactionService }) => ({
    execute: async (id) => ({ balance: await transactionService.getBalanceByUserId(id) })
});
