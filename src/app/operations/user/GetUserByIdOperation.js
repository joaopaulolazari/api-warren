module.exports = ({ userService }) => ({
    execute: (id) => userService.getUserById(id)
});
