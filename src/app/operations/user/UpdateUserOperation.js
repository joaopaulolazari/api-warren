module.exports = ({ userService }) => ({
    execute: (id, data) => userService.update(id, data)
});
