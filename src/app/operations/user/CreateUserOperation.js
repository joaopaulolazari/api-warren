module.exports = ({ userService }) => ({
    execute: data => userService.create(data)
});
