module.exports = ({ userService }) => ({
    execute: (id) => userService.delete(id)
});
