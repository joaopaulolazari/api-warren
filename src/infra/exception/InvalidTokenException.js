const Exception = require('./Exception');
const defaultErrorCode = '401';

module.exports = class InvalidTokenException extends Exception {
    constructor(error, appCode, ...params) {
        super(error, defaultErrorCode, appCode, ...params);
        this.error_type = 'contract';
    }
};
