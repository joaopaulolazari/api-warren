'use strict';
const { Schema } = require('mongoose');
const paginate = require('mongoose-paginate');
const { Model } = require('src/infra/support/libs/connection-database/src');

module.exports = class TransactionModel extends Model {

    constructor({ providerConnection, config }) {
        const { name: collectionName, version: collectionVersion } = config.db.collections.transaction;
        super({ providerConnection, collectionName, collectionVersion });
    }

    getSchema() {
        const transactionSchema = new Schema({
            transaction_id: {
                type: String,
                required: true
            },
            user_id: {
                type: String,
                required: true
            },
            type: {
                type: String,
                enum : ['deposit', 'payment', 'discharge'],
                required: true
            },
            value: {
                type: Number,
                required: true
            },
        });

        transactionSchema.plugin(paginate);

        transactionSchema.index(
            { transaction_id: 1 },
            { unique: true }
        );

        return transactionSchema;
    }
};
