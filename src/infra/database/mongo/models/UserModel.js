'use strict';
const { Schema } = require('mongoose');
const paginate = require('mongoose-paginate');
const { Model } = require('src/infra/support/libs/connection-database/src');

module.exports = class UserModel extends Model {

    constructor({ providerConnection, config }) {
        const { name: collectionName, version: collectionVersion } = config.db.collections.user;
        super({ providerConnection, collectionName, collectionVersion });
    }

    getSchema() {
        const userSchema = new Schema({
            user_id: {
                type: String,
                required: true
            },
            name: {
                type: String,
                required: true
            },
        });

        userSchema.plugin(paginate);

        userSchema.index(
            { user_id: 1 },
            { unique: true }
        );

        return userSchema;
    }
};
