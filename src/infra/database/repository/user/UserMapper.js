const UserMapper = {

    toEntity: ({ user_id, name }) => ({ user_id, name }),

    toDatabase: domainEntity => {
        return domainEntity;
    }
};

module.exports = UserMapper;
