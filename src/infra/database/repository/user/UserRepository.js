const { Repository } = require('src/infra/support/libs/connection-database/src');
const UserMapper = require('src/infra/database/repository/user/UserMapper');
const EnumMessage = require('src/domain/enum/EnumMessage');
const NotFoundException = require('src/infra/exception/NotFoundException');

class UserRepository extends Repository {
    constructor({ userModel, logger, appCode }) {
        super({
            ResourceModel: userModel,
            ResourceMapper: UserMapper,
            logger
        });
        this.appCode = appCode;
    }

    async getUser(user_id) {
        const res = await this.get({ user_id });

        if (!res)
            throw new NotFoundException(EnumMessage.USER_NOT_FOUND, this.appCode);

        return res;
    }

    async updateUser(user_id, data) {
        const res = await this.update({ user_id }, data);

        if (!res)
            throw new NotFoundException(EnumMessage.USER_NOT_FOUND, this.appCode);

        return res;
    }

    async deleteUser(user_id) {
        const res = await this.remove({ user_id });

        if (!res)
            throw new NotFoundException(EnumMessage.USER_NOT_FOUND, this.appCode);

        return res;
    }
}

module.exports = UserRepository;
