const { Repository } = require('src/infra/support/libs/connection-database/src');
const TransactionMapper = require('src/infra/database/repository/transaction/TransactionMapper');
const EnumMessage = require('src/domain/enum/EnumMessage');
const NotFoundException = require('src/infra/exception/NotFoundException');

class TransactionRepository extends Repository {
    constructor({ transactionModel, logger, appCode }) {
        super({
            ResourceModel: transactionModel,
            ResourceMapper: TransactionMapper,
            logger
        });
        this.appCode = appCode;
    }

    async getByUserId(user_id) {
        return await this.find({ user_id }, { created_at: -1 });
    }

    async getTransaction(transaction_id) {
        const res = await this.get({ transaction_id });

        if (!res)
            throw new NotFoundException(EnumMessage.TRANSACTION_NOT_FOUND, this.appCode);

        return res;
    }

    async updateTransaction(transaction_id, data) {
        const res = await this.update({ transaction_id }, data);

        if (!res)
            throw new NotFoundException(EnumMessage.TRANSACTION_NOT_FOUND, this.appCode);

        return res;
    }

    async deleteTransaction(transaction_id) {
        const res = await this.remove({ transaction_id });

        if (!res)
            throw new NotFoundException(EnumMessage.TRANSACTION_NOT_FOUND, this.appCode);

        return res;
    }
}

module.exports = TransactionRepository;
