const TransactionMapper = {
    toEntity: entity => entity,
    toDatabase: entity => entity
};

module.exports = TransactionMapper;
