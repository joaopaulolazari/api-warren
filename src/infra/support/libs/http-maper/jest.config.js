module.exports = {
    verbose: true,
    moduleNameMapper: {
        '^@\\/(.*)$': '<rootDir>/src/$1'
    },
    coveragePathIgnorePatterns: ['/test/', '/node_modules/'],
    testPathIgnorePatterns: ['/node_modules/'],
    transform: {
        '^.+\\.js$': 'babel-jest'
    },
    coverageReporters: ['cobertura', 'text', 'html'],
    reporters: ['default', 'jest-junit'],
    coverageThreshold: {
        global: {
            branches: 100,
            functions: 100,
            lines: 100,
            statements: 100
        }
    }
};
