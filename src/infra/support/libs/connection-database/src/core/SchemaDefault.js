const { Schema } = require('mongoose');

const _dateFormat = (date) => {
    return new Date(date).toISOString();
};

const schemaDefault = new Schema({
    created_at: {
        type: Date,
        set: (el) => _dateFormat(el)
    },
    updated_at: {
        type: Date,
        set: (el) => _dateFormat(el)
    },
    deleted_at: {
        type: Date,
        set: (el) => _dateFormat(el)
    },
    is_deleted: {
        type: Boolean,
        default: false
    },
    schema_version: {
        type: String,
        required: true,
        default: '1.0.0'
    }
}, {
    timestamps: {
        currentTime: () => _dateFormat(Date.now()),
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

schemaDefault.indexes({ is_deleted: 1 });

module.exports = schemaDefault;