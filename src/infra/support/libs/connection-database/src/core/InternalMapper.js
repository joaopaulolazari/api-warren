class InternalMapper {

    constructor(ResourceMapper, clientInformations) {
        this.ResourceMapper = ResourceMapper;
        this.clientInformations = clientInformations;
    }

    toEntity(resourceEntity) {
        const { created_at, updated_at, deleted_at, is_deleted, schema_version } = resourceEntity;
        const data = this.ResourceMapper.toEntity(resourceEntity);
        Object.assign(data, { created_at, updated_at, deleted_at, is_deleted, schema_version });

        return data;
    }

    toDatabase(entity) {
        return this.ResourceMapper.toDatabase(entity);
    }
} module.exports = InternalMapper;
