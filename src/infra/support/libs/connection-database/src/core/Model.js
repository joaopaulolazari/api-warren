const SchemaDefault = require('./SchemaDefault');

module.exports = class Model {
    constructor({ providerConnection, collectionName, collectionVersion }) {
        this.collectionName = collectionName;
        this.collectionVersion = collectionVersion;
        this.validateConfig();
        const model = this.getSchema();
        model.add(SchemaDefault);

        this.model = providerConnection.connection.model(this.collectionName, model);
    }

    validateConfig() {
        if (!this.collectionName)
            throw new Error('Param constructor (collectionName) is required.');

        if (!this.collectionVersion)
            throw new Error('Param constructor (collectionVersion) is required.');
    }

    getSchema() {
        throw new Error('You must override this method "getSchema".');
    }
};
