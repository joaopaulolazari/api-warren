const DUPLICATE_KEY_ERROR_CODE = 11000;
const clear = require('../libs/utils/ClearObject');
const InternalMapper = require('./InternalMapper');

class Repository {
    constructor({
        ResourceModel,
        ResourceMapper,
        logger,
        clientInformations
    }) {
        this.ResourceModel = ResourceModel.model;
        this.isGlobal = ResourceModel.isGlobal;
        this.collectionVersion = ResourceModel.collectionVersion;
        this.logger = logger;
        this.clientInformations = clientInformations;
        this.internalMapper = new InternalMapper(ResourceMapper, clientInformations);
    }

    async count(query = {}) {
        query = this._query(query);
        return this.ResourceModel.countDocuments(query);
    }

    async get(query) {
        query = this._query(query);
        const databaseResource = await this.ResourceModel.findOne(query);

        if (!databaseResource || databaseResource === {}) return null;

        return this.internalMapper.toEntity(databaseResource);
    }

    async create(entity) {
        entity = this._entity(entity);

        try {
            const resourceModel = this.internalMapper.toDatabase(entity);

            const databaseCreatedResource = await this.ResourceModel.create(resourceModel);
            return this.internalMapper.toEntity(databaseCreatedResource);
        } catch (error) {
            await this._throwError(error);
        }
    }

    async update(
        query,
        entity,
        options = { new: true, upsert: false, runValidators: true }
    ) {
        query = this._query(query);
        entity = this._entity(entity);

        try {
            const entityDatabase = this.internalMapper.toDatabase(entity);

            const databaseUpdatedResource = await this.ResourceModel.findOneAndUpdate(
                query,
                entityDatabase,
                options
            );
            if (!databaseUpdatedResource) return null;

            return this.internalMapper.toEntity(databaseUpdatedResource);
        } catch (error) {
            await this._throwError(error);
        }
    }

    async replace(query, entity, options = { new: true, returnOriginal: false, runValidators: true }) {
        query = this._query(query);
        entity = this._entity(entity);

        const entityDatabase = this.internalMapper.toDatabase(entity);

        try {
            const databaseReplacedResource = await this.ResourceModel.findOneAndReplace(
                query,
                entityDatabase,
                options
            );

            return this.internalMapper.toEntity(databaseReplacedResource);
        } catch (error) {
            await this._throwError(error);
        }
    }

    async remove(query) {
        const document = await this.get(query);

        if (!document) {
            this.logger.error('Document not found.');
            return false;
        }

        document.is_deleted = true;
        document.deleted_at = new Date().toISOString();

        await this.update(query, document);

        return true;
    }

    async findPaginated({
        page = 1,
        limit = 100,
        query = {},
        clearQuery = true
    }) {
        if (clearQuery) clear(query);
        query = this._query(query);

        const option = { page: Number(page), limit: Number(limit) };
        const result = await this.ResourceModel.paginate(query, option);

        result.docs = result.docs.map(doc =>
            this.internalMapper.toEntity(doc)
        );

        return result;
    }

    async find(query = {}, sort = { _id: 'desc' }) {
        query = this._query(query);

        let result = await this.ResourceModel.find(query).sort(sort);

        if (!result || result === {} || result.length === 0) return [];

        result = result.map(doc =>
            this.internalMapper.toEntity(doc)
        );

        return result;
    }

    async aggregate(pipeline) {
        return await this.ResourceModel.aggregate(pipeline);
    }

    _multiTenant(data) {
        return data;
    }

    _query(query) {
        query.is_deleted = false;
        return this._multiTenant(query);
    }

    _entity(entity) {
        return this._multiTenant(Object.assign(entity, { schema_version: this.collectionVersion }));
    }

    _throwError(error) {
        this.logger.error(error);
        let message = error;

        if (error.code === DUPLICATE_KEY_ERROR_CODE) {
            message = 'Duplicate key error';
        }

        throw new Error(message);
    }
}
module.exports = Repository;
