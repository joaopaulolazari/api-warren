module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


const SwaggerDocGenerator = __webpack_require__(/*! ./lib/SwaggerDocGenerator */ "./src/lib/SwaggerDocGenerator.js");

module.exports = SwaggerDocGenerator;

/***/ }),

/***/ "./src/lib/JoiToSwagger.js":
/*!*********************************!*\
  !*** ./src/lib/JoiToSwagger.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


const find = (items, query) => {
  if (!items || !items.length) return false;
  return items.find(item => Object.keys(query).every(key => item[key] === query[key]));
};

const meta = (schema, key) => {
  const meta = schema.metas && schema.metas.find(meta => meta[key] !== undefined);
  return meta && meta[key];
};

const merge = Object.assign;
const patterns = {
  alphanum: '^[a-zA-Z0-9]*$',
  alphanumLower: '^[a-z0-9]*$',
  alphanumUpper: '^[A-Z0-9]*$'
};

const getMinMax = (schema, suffix = 'Length') => {
  const swagger = {};
  if (!schema.rules) return swagger;

  for (let i = 0; i < schema.rules.length; i++) {
    const test = schema.rules[i];

    if (test.name === 'min') {
      swagger[`min${suffix}`] = test.args.limit;
    }

    if (test.name === 'max') {
      swagger[`max${suffix}`] = test.args.limit;
    }

    if (test.name === 'length') {
      swagger[`min${suffix}`] = test.args.limit;
      swagger[`max${suffix}`] = test.args.limit;
    }
  }

  return swagger;
};

const getCaseSuffix = schema => {
  const caseRule = find(schema.rules, {
    name: 'case'
  });

  if (caseRule && caseRule.args.direction === 'lower') {
    return 'Lower';
  } else if (caseRule && caseRule.args.direction === 'upper') {
    return 'Upper';
  }

  return '';
};

const parseAsType = {
  number: schema => {
    const swagger = {};

    if (find(schema.rules, {
      name: 'integer'
    })) {
      swagger.type = 'integer';
    } else {
      swagger.type = 'number';

      if (find(schema.rules, {
        name: 'precision'
      })) {
        swagger.format = 'double';
      } else {
        swagger.format = 'float';
      }
    }

    const sign = find(schema.rules, {
      name: 'sign'
    });

    if (sign) {
      if (sign.args.sign === 'positive') {
        swagger.minimum = 1;
      } else if (sign.args.sign === 'negative') {
        swagger.maximum = -1;
      }
    }

    const min = find(schema.rules, {
      name: 'min'
    });

    if (min) {
      swagger.minimum = min.args.limit;
    }

    const max = find(schema.rules, {
      name: 'max'
    });

    if (max) {
      swagger.maximum = max.args.limit;
    }

    if (schema.allow) {
      const valids = schema.allow.filter(s => typeof s === 'number');

      if (schema.flags && schema.flags.only && valids.length) {
        swagger.enum = valids;
      }
    }

    return swagger;
  },
  string: schema => {
    const swagger = {
      type: 'string'
    };

    if (find(schema.rules, {
      name: 'alphanum'
    })) {
      const strict = (schema.preferences && schema.preferences.convert) === false;
      swagger.pattern = patterns[`alphanum${strict ? getCaseSuffix(schema) : ''}`];
    }

    if (find(schema.rules, {
      name: 'token'
    })) {
      swagger.pattern = patterns[`alphanum${getCaseSuffix(schema)}`];
    }

    if (find(schema.rules, {
      name: 'email'
    })) {
      swagger.format = 'email';
      if (swagger.pattern) delete swagger.pattern;
    }

    if (find(schema.rules, {
      name: 'isoDate'
    })) {
      swagger.format = 'date-time';
      if (swagger.pattern) delete swagger.pattern;
    }

    if (find(schema.rules, {
      name: 'guid'
    })) {
      swagger.format = 'uuid';
      if (swagger.pattern) delete swagger.pattern;
    }

    const pattern = find(schema.rules, {
      name: 'pattern'
    });

    if (pattern) {
      swagger.pattern = pattern.args.regex.toString().slice(1, -1);
    }

    Object.assign(swagger, getMinMax(schema));

    if (schema.allow) {
      const valids = schema.allow.filter(s => typeof s === 'string');

      if (schema.flags && schema.flags.only && valids.length) {
        swagger.enum = valids;
      }
    }

    return swagger;
  },
  binary: schema => {
    const swagger = {
      type: 'string',
      format: 'binary'
    };

    if ((schema.flags && schema.flags.encoding) === 'base64') {
      swagger.format = 'byte';
    }

    Object.assign(swagger, getMinMax(schema));
    return swagger;
  },
  date: () => ({
    type: 'string',
    format: 'date-time'
  }),
  boolean: () => ({
    type: 'boolean'
  }),
  alternatives: (schema, existingComponents, newComponentsByRef) => {
    const index = meta(schema, 'swaggerIndex') || 0;
    const matches = schema.matches || [];
    const firstItem = matches[index];
    let itemsSchema;

    if (firstItem.ref) {
      if (schema._baseType && !firstItem.otherwise) {
        itemsSchema = index ? firstItem.then : schema.baseType;
      } else {
        itemsSchema = index ? firstItem.otherwise : firstItem.then;
      }
    } else if (index) {
      itemsSchema = matches[index].schema;
    } else {
      itemsSchema = firstItem.schema;
    }

    const items = parseDescription(itemsSchema, merge({}, existingComponents || {}, newComponentsByRef || {}));

    if ((itemsSchema.flags && itemsSchema.flags.presence) === 'required') {
      items.swagger.__required = true;
    }

    merge(newComponentsByRef, items.components || {});
    return items.swagger;
  },
  array: (schema, existingComponents, newComponentsByRef) => {
    const index = meta(schema, 'swaggerIndex') || 0;
    const itemsSchema = schema.items && schema.items[index] || {
      type: 'any'
    };
    if (!itemsSchema) throw Error('Array schema does not define an items schema at index ' + index);
    const items = parseDescription(itemsSchema, merge({}, existingComponents || {}, newComponentsByRef || {}));
    merge(newComponentsByRef, items.components || {});
    const swagger = {
      type: 'array'
    };
    Object.assign(swagger, getMinMax(schema, 'Items'));

    if (find(schema.rules, {
      name: 'unique'
    })) {
      swagger.uniqueItems = true;
    }

    swagger.items = items.swagger;
    return swagger;
  },
  object: (schema, existingComponents, newComponentsByRef) => {
    const requireds = [];
    const properties = {};
    const combinedComponents = merge({}, existingComponents || {}, newComponentsByRef || {});
    const children = schema.keys || {};
    Object.keys(children).forEach(key => {
      const child = children[key];
      const prop = parseDescription(child, combinedComponents);

      if (!prop.swagger) {
        // swagger is falsy if joi.forbidden()
        return;
      }

      merge(newComponentsByRef, prop.components || {});
      merge(combinedComponents, prop.components || {});
      properties[key] = prop.swagger;

      if ((child.flags && child.flags.presence) === 'required' || prop.swagger.__required) {
        requireds.push(key);
        delete prop.swagger.__required;
      }
    });
    const swagger = {
      type: 'object'
    };
    if (requireds.length) swagger.required = requireds;
    swagger.properties = properties;
    if ((schema.flags && schema.flags.unknown) !== true) swagger.additionalProperties = false;
    return swagger;
  },
  any: schema => {
    const swagger = {}; // convert property to file upload, if indicated by meta property

    if (meta(schema, 'swaggerType') === 'file') {
      swagger.type = 'file';
      swagger.in = 'formData';
    }

    return swagger;
  }
};

function parseDescription(schemaDescription, existingComponents) {
  if ((schemaDescription.flags && schemaDescription.flags.presence) === 'forbidden') return false;
  const type = schemaDescription.baseType || schemaDescription.type;
  if (!parseAsType[type]) throw new TypeError(`${type} is not a recognized Joi type.`);
  const components = {};
  const swagger = parseAsType[type](schemaDescription, existingComponents, components);
  if (!swagger) return {
    swagger,
    components
  };
  if (schemaDescription.allow && schemaDescription.allow.includes(null)) swagger.nullable = true;
  const description = schemaDescription.flags && schemaDescription.flags.description;
  if (description) swagger.description = description;

  if (schemaDescription.examples) {
    if (schemaDescription.examples.length === 1) {
      swagger.example = schemaDescription.examples[0];
    } else {
      swagger.examples = schemaDescription.examples;
    }
  }

  const label = schemaDescription.flags && schemaDescription.flags.label;
  if (label) swagger.title = label;
  const defaultValue = schemaDescription.flags && schemaDescription.flags.default;

  if (defaultValue && typeof defaultValue !== 'function') {
    swagger.default = defaultValue;
  }

  return {
    swagger,
    components
  };
}

function parseJoiSchema(schema) {
  let schemaDescription = {};
  if (!schema) throw new Error('No schema is specified');
  schemaDescription = schema.describe();
  return parseDescription(schemaDescription);
}

module.exports = {
  parseJoiSchema
};

/***/ }),

/***/ "./src/lib/SwaggerDocGenerator.js":
/*!****************************************!*\
  !*** ./src/lib/SwaggerDocGenerator.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


const swaggerJsdoc = __webpack_require__(/*! swagger-jsdoc */ "swagger-jsdoc");

const {
  parseJoiSchema
} = __webpack_require__(/*! ./JoiToSwagger */ "./src/lib/JoiToSwagger.js");

const defaultOptions = {
  title: 'API swagger',
  version: 'v1',
  description: 'Auto generated api swagger',
  basePath: '/api',
  schemes: ['http']
};
const parseValidation = {
  body: item => {
    return [{
      in: item.type,
      required: true,
      name: item.type,
      schema: parseJoiSchema(item.parameter).swagger
    }];
  },
  query: item => {
    const {
      properties,
      required
    } = parseJoiSchema(item.parameter).swagger;
    return Object.keys(properties).map(propertyKey => {
      return {
        name: propertyKey,
        in: item.type,
        required: required && required.includes(propertyKey),
        type: properties[propertyKey].type
      };
    });
  },
  params: item => {
    const {
      properties,
      required
    } = parseJoiSchema(item.parameter).swagger;
    return Object.keys(properties).map(propertyKey => {
      return {
        name: propertyKey,
        in: 'path',
        required: required && required.includes(propertyKey),
        type: properties[propertyKey].type
      };
    });
  },
  headers: item => {
    const {
      properties,
      required
    } = parseJoiSchema(item.parameter).swagger;
    return Object.keys(properties).map(propertyKey => {
      return {
        name: propertyKey,
        in: 'header',
        required: required && required.includes(propertyKey),
        type: properties[propertyKey].type
      };
    });
  }
};

const parseParameters = arrParameters => {
  let parameters = [];
  arrParameters.forEach(item => {
    parameters = parameters.concat(parseValidation[item.type](item));
  });
  return parameters;
};

const generateSwagger = (routes, options = defaultOptions) => {
  try {
    const {
      title = defaultOptions.title,
      version = defaultOptions.version,
      description = defaultOptions.description,
      basePath = defaultOptions.basePath,
      schemes = defaultOptions.schemes
    } = options;
    const paths = {};
    routes.forEach(route => {
      let {
        method,
        path,
        description = '',
        validation,
        tags = []
      } = route;
      const {
        body,
        headers,
        params,
        query
      } = validation;
      const arrParameters = [{
        type: 'body',
        parameter: body
      }, {
        type: 'headers',
        parameter: headers
      }, {
        type: 'params',
        parameter: params
      }, {
        type: 'query',
        parameter: query
      }].filter(item => item.parameter !== undefined);

      if (path.includes(':')) {
        const params = path.match(/:[-a-zA-Z0-9@:%._+~#=]+/g);
        params.forEach(param => {
          path = path.replace(param, `{${param.replace(':', '')}}`);
        });
      }

      if (!Array.isArray(tags)) {
        tags = [];
      }

      paths[path] = paths[path] || {};
      paths[path][method] = {
        consumes: ['application/json'],
        produces: ['application/json'],
        description,
        tags,
        parameters: parseParameters(arrParameters),
        responses: {}
      };
    });
    const swaggerDoc = {
      swaggerDefinition: {
        info: {
          title,
          version,
          description
        },
        basePath,
        schemes,
        paths
      },
      apis: []
    };
    return swaggerJsdoc(swaggerDoc);
  } catch (error) {
    console.warn(error);
    const {
      title,
      version,
      description,
      basePath,
      schemes
    } = defaultOptions;
    return swaggerJsdoc({
      swaggerDefinition: {
        info: {
          title,
          version,
          description
        },
        basePath,
        schemes
      },
      apis: []
    });
  }
};

module.exports = {
  generateSwagger
};

/***/ }),

/***/ "swagger-jsdoc":
/*!********************************!*\
  !*** external "swagger-jsdoc" ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("swagger-jsdoc");

/***/ })

/******/ });
//# sourceMappingURL=main.js.map