const nodeExternals = require('webpack-node-externals')
const {
  resolve
} = require('path')

module.exports = {
  mode: 'development',
  target: 'node',
  devtool: 'source-map',
  entry: {
    main: resolve(__dirname, '../src/index.js')
  },
  output: {
    path: resolve(__dirname, '../lib'),
    filename: '[name].js',
    libraryTarget: 'commonjs2'
  },
  node: {
    __filename: true,
    __dirname: true
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      use: 'babel-loader'
    }]
  },
  externals: [nodeExternals()]
}
