module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/appenders/applicationInsights/AppInsightsAppender.js":
/*!******************************************************************!*\
  !*** ./src/appenders/applicationInsights/AppInsightsAppender.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


const {
  ERROR
} = __webpack_require__(/*! log4js */ "log4js").levels;

const LevelMapper = __webpack_require__(/*! ./LevelMapper */ "./src/appenders/applicationInsights/LevelMapper.js");

const {
  formatLoggingEventData
} = __webpack_require__(/*! ../../formatters/LogFormatter */ "./src/formatters/LogFormatter.js");

const {
  toTrace,
  toException
} = __webpack_require__(/*! ./AppInsightsFactory */ "./src/appenders/applicationInsights/AppInsightsFactory.js");
/**
 * @description Get an array of formated details log message.
 * @param {object} loggingEventData The logger event data.
 * @returns {Array} The formated details.
 */


const getLoggingDetails = loggingEventData => {
  return loggingEventData.map(item => formatLoggingEventData(item));
};
/**
 * @description Send a logger message to applications insights traces.
 * @param {Function} layout The log4js layout formatter.
 * @param {object} loggingEvent The logger event.
 * @param {object} appInsightsClient The application insights client.
 * @param {Function} appInsightsClient.trackTrace The application insights track trace function.
 */


const sendTrace = (layout, loggingEvent, appInsightsClient) => {
  const {
    level,
    startTime,
    data
  } = loggingEvent; // get logging event data details

  const details = getLoggingDetails(data); // send log event to appinsights traces

  const traceLog = toTrace(layout(loggingEvent), LevelMapper[level], startTime, details);
  appInsightsClient.trackTrace(traceLog);
};
/**
 * @description Send the error to applications insights exceptions.
 * @param {object} loggingEvent The logger event.
 * @param {object[]} loggingEvent.data The logging event data.
 * @param {object} appInsightsClient The application insights client.
 * @param {Function} appInsightsClient.trackException The application insights track exception function.
 * @returns {Error[]} Returns an array of errors sended to application insights.
 */


const sendException = (loggingEvent, appInsightsClient) => {
  // just send to application insights exceptions if an error
  if (loggingEvent.level !== ERROR) return [];
  const errors = loggingEvent.data.filter(item => item instanceof Error);
  if (errors.length) errors.forEach(exception => appInsightsClient.trackException(toException(exception, loggingEvent.startTime, formatLoggingEventData(exception))));
  return errors;
};
/**
 * @description The logger appender function.
 * @param {Function} layout The log4js layout formatter.
 * @param {object} appInsights The application insights instance.
 * @param {object} appInsights.defaultClient The application insights client.
 * @param {Function} appInsights.defaultClient.trackTrace The application insights track trace function.
 * @param {Function} appInsights.defaultClient.trackException The application insights track exception function.
 * @returns {Function} The logging event function handler.
 */


const loggingAppender = (layout, appInsights) => loggingEvent => {
  if (!appInsights.defaultClient) return null;
  sendTrace(layout, loggingEvent, appInsights.defaultClient);
  sendException(loggingEvent, appInsights.defaultClient);
};
/**
 * @description Get a new application insights appender instance.
 * @param {object} appInsights The application insights instance.
 * * @param {object} appInsights.defaultClient The application insights client.
 * @param {Function} appInsights.defaultClient.trackTrace The application insights track trace function.
 * @param {Function} appInsights.defaultClient.trackException The application insights track exception function.
 * @param {string} [layout=messagePassThrough] - The log format layout to be used, default is 'messagePassThrough'.
 * @returns {object} Returns que application insights appender instance.
 */


module.exports = (appInsights, layout) => ({
  type: {
    configure: (config, layouts) => {
      const layout = layouts.layout(config.layout);
      return loggingAppender(layout, appInsights);
    }
  },
  layout: layout || 'messagePassThrough'
});

/***/ }),

/***/ "./src/appenders/applicationInsights/AppInsightsFactory.js":
/*!*****************************************************************!*\
  !*** ./src/appenders/applicationInsights/AppInsightsFactory.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = {
  toTrace: (message, severity, dateTime, details = {}) => ({
    message,
    severity,
    properties: {
      timestamp: dateTime.toISOString(),
      details
    }
  }),
  toException: (exception, dateTime, details = {}) => ({
    exception,
    properties: {
      timestamp: dateTime.toISOString(),
      details
    }
  })
};

/***/ }),

/***/ "./src/appenders/applicationInsights/LevelMapper.js":
/*!**********************************************************!*\
  !*** ./src/appenders/applicationInsights/LevelMapper.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


const {
  ALL,
  TRACE,
  DEBUG,
  INFO,
  WARN,
  ERROR,
  FATAL,
  MARK
} = __webpack_require__(/*! log4js */ "log4js").levels;

const InsightsLevel = {
  Verbose: 0,
  Information: 1,
  Warning: 2,
  Error: 3,
  Critical: 4
};
const level = {
  [ALL]: InsightsLevel.Verbose,
  [TRACE]: InsightsLevel.Verbose,
  [DEBUG]: InsightsLevel.Verbose,
  [INFO]: InsightsLevel.Information,
  [WARN]: InsightsLevel.Warning,
  [ERROR]: InsightsLevel.Error,
  [FATAL]: InsightsLevel.Critical,
  [MARK]: InsightsLevel.Critical
};
module.exports = level;

/***/ }),

/***/ "./src/appenders/standard/StandardAppender.js":
/*!****************************************************!*\
  !*** ./src/appenders/standard/StandardAppender.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


const configure = () => ({
  appenders: {
    out: {
      type: 'stdout',
      layout: {
        type: 'coloured'
      }
    }
  },
  categories: {
    default: {
      appenders: ['out'],
      level: 'all'
    }
  }
});

module.exports = {
  configure
};

/***/ }),

/***/ "./src/formatters/LogFormatter.js":
/*!****************************************!*\
  !*** ./src/formatters/LogFormatter.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


const {
  serializeError,
  serializeStackTrace
} = __webpack_require__(/*! ./LogSerializer */ "./src/formatters/LogSerializer.js");

const formatLoggingEventData = loggingEventData => {
  if (!(loggingEventData instanceof Error)) return loggingEventData;
  const stack = loggingEventData.stack_trace || loggingEventData.stack;
  return { ...serializeError(loggingEventData),
    ...serializeStackTrace(stack)
  };
};

module.exports = {
  formatLoggingEventData
};

/***/ }),

/***/ "./src/formatters/LogSerializer.js":
/*!*****************************************!*\
  !*** ./src/formatters/LogSerializer.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// eslint-disable-next-line camelcase
const serializeError = ({
  message,
  error_type,
  error_code,
  details
}) => ({
  message,
  error_type,
  error_code,
  details
});

const serializeStackTrace = stackTrace => ({
  type: stackTrace.split('\n')[0].split(':').shift().trim(),
  file: stackTrace.split('\n')[1].trim().split(' ').pop(),
  stack_trace: stackTrace
});

module.exports = {
  serializeError,
  serializeStackTrace
};

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = __webpack_require__(/*! ./lib/Logger */ "./src/lib/Logger.js");

/***/ }),

/***/ "./src/lib/ConfigBuilder.js":
/*!**********************************!*\
  !*** ./src/lib/ConfigBuilder.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


const {
  configure
} = __webpack_require__(/*! ../appenders/standard/StandardAppender */ "./src/appenders/standard/StandardAppender.js");

const AppInsightsAppender = __webpack_require__(/*! ../appenders/applicationInsights/AppInsightsAppender */ "./src/appenders/applicationInsights/AppInsightsAppender.js");

const buildConfig = (config = {}) => {
  const logConfig = configure();
  Object.assign(logConfig.appenders, config.appenders);

  if (logConfig.appenders.appInsights) {
    const {
      instance,
      layout
    } = logConfig.appenders.appInsights;
    logConfig.appenders.appInsights = AppInsightsAppender(instance, layout);
    logConfig.categories.default.appenders.push('appInsights');
  }

  return logConfig;
};

module.exports = {
  buildConfig
};

/***/ }),

/***/ "./src/lib/Logger.js":
/*!***************************!*\
  !*** ./src/lib/Logger.js ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


const log4js = __webpack_require__(/*! log4js */ "log4js");

const {
  buildConfig
} = __webpack_require__(/*! ./ConfigBuilder */ "./src/lib/ConfigBuilder.js");

module.exports = config => {
  const logConfig = buildConfig(config);
  log4js.configure(logConfig);
  return log4js.getLogger();
};

/***/ }),

/***/ "log4js":
/*!*************************!*\
  !*** external "log4js" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("log4js");

/***/ })

/******/ });
//# sourceMappingURL=main.js.map