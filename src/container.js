const {
    createContainer,
    asClass,
    asFunction,
    asValue,
    InjectionMode,
    Lifetime
} = require('awilix');

const Server = require('./interfaces/http/Server');
const Router = require('./interfaces/http/Router');
const logger = require('./infra/logging/logger');

const { ProviderConnection } = require('src/infra/support/libs/connection-database/src');
const { HttpClientMapper, HttpClientHandler, HttpConstants } = require('src/infra/support/libs/http-maper/src');

const container = createContainer();

const configureContainer = config => {
    container
        .register({
            messageActionEnum: asValue(require('src/domain/enum/MessageActionEnum')),
            server: asClass(Server).singleton(),
            router: asFunction(Router),
            logger: asFunction(logger).singleton(),
            container: asValue(container),
            config: asValue(config),
            appCode: asValue(config.appCode),
            providerConnection: asClass(ProviderConnection).singleton(),
            httpClientMapper: asFunction(HttpClientMapper),
            httpClientHandler: asFunction(HttpClientHandler),
            httpConstants: asValue(HttpConstants)
        })
        .loadModules(
            [
                'src/app/topics/**/*.js',
                [
                    'src/infra/database/mongo/models/**/*.js',
                    {
                        lifetime: Lifetime.SINGLETON
                    }
                ],
                'src/infra/database/repository/**/*.js',
                'src/infra/integration/**/*.js',
                'src/app/operations/**/*.js',
                'src/app/services/**/*.js',
                'src/domain/services/**/*.js',
                'src/domain/schemas/**/*.js',
                'src/interfaces/http/errors/**/*.js',
                'src/interfaces/http/middlewares/**/*.js',
                'src/interfaces/http/presentation/**/*.js'
            ],
            {
                formatName: 'camelCase',
                resolverOptions: {
                    injectionMode: InjectionMode.PROXY
                }
            }
        );

    return container;
};


module.exports = { configureContainer, container };
