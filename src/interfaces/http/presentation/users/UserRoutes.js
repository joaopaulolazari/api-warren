module.exports = ({ userSchema, userController }) => {
    return [
        {
            method: 'get',
            path: '/users/:id',
            tags: ['Users'],
            validation: {
                params: userSchema.get
            },
            handler: userController.get
        },
        {
            method: 'post',
            path: '/users',
            tags: ['Users'],
            validation: {
                body: userSchema.post
            },
            handler: userController.post
        },
        {
            method: 'put',
            path: '/users/:id',
            tags: ['Users'],
            validation: {
                params: userSchema.get,
                body: userSchema.update
            },
            handler: userController.update
        },
        {
            method: 'delete',
            path: '/users/:id',
            tags: ['Users'],
            validation: {
                params: userSchema.get,
            },
            handler: userController.delete
        }
    ];
};
