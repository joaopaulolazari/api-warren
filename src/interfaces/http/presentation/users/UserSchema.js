const joi = require('joi');

module.exports = () => ({
    get: joi.object().keys({
        id: joi.string().uuid({ version: 'uuidv4' }).required()
    }),
    post: joi.object().keys({
        name: joi.string().required()
    }),
    delete: joi.object().keys({
        id: joi.string().uuid({ version: 'uuidv4' }).required()
    }),
    update: joi.object().keys({
        name: joi.string().required()
    })
});

