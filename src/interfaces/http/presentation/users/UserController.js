const AsyncMiddleware = require('src/interfaces/http/middlewares/AsyncMiddleware');

module.exports = opts => ({

    get: AsyncMiddleware(async ctx => {
        const { params: { id } } = ctx;
        const res = await opts.getUserByIdOperation.execute(id);

        return ctx.res.status(opts.httpConstants.code.OK).json(res);
    }),

    post: AsyncMiddleware(async ctx => {
        const { body } = ctx;
        const res = await opts.createUserOperation.execute({ ...body });

        return ctx.res.status(opts.httpConstants.code.CREATED).json(res);
    }),

    update: AsyncMiddleware(async ctx => {
        const { body, params: { id } } = ctx;
        const res = await opts.updateUserOperation.execute(id, body);

        return ctx.res.status(opts.httpConstants.code.OK).json(res);
    }),

    delete: AsyncMiddleware(async ctx => {
        const { params: { id } } = ctx;
        await opts.deleteUserOperation.execute(id);
        return ctx.res.status(opts.httpConstants.code.NO_CONTENT).send();
    })
});
