module.exports = ({ balanceSchema, balanceController }) => {
    return [
        {
            method: 'get',
            path: '/balance',
            tags: ['Balance'],
            validation: {
                headers: balanceSchema.headers,
            },
            handler: balanceController.get
        }
    ];
};
