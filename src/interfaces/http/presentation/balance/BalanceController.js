const AsyncMiddleware = require('src/interfaces/http/middlewares/AsyncMiddleware');

module.exports = opts => ({
    get: AsyncMiddleware(async ctx => {
        const { user_id: userId } = ctx;
        const res = await opts.getBalanceByUserOperation.execute(userId);

        return ctx.res.status(opts.httpConstants.code.OK).json(res);
    })
});
