const joi = require('joi');

module.exports = () => ({
    headers: joi.object().keys({
        authorization: joi.string().required()
    })
});

