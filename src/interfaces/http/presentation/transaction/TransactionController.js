const AsyncMiddleware = require('src/interfaces/http/middlewares/AsyncMiddleware');

module.exports = opts => ({
    post: AsyncMiddleware(async ctx => {
        const { body, user_id } = ctx;
        const res = await opts.createTransactionOperation.execute({ ...body, user_id });

        return ctx.res.status(opts.httpConstants.code.CREATED).json(res);
    }),
    getByUserId: AsyncMiddleware(async ctx => {
        const { user_id: userId } = ctx;
        const res = await opts.getTransactionsByUserIdOperation.execute(userId);

        return ctx.res.status(opts.httpConstants.code.OK).json(res);
    })
});
