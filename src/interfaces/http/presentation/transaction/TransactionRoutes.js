module.exports = ({ transactionSchema, transactionController }) => {
    return [
        {
            method: 'post',
            path: '/transactions',
            tags: ['Transactions'],
            validation: {
                body: transactionSchema.post,
                headers: transactionSchema.headers
            },
            handler: transactionController.post
        },
        {
            method: 'get',
            path: '/transactions',
            tags: ['Transactions'],
            validation: {
                headers: transactionSchema.headers
            },
            handler: transactionController.getByUserId
        }
    ];
};
