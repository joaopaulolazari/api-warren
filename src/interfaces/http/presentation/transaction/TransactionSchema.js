const joi = require('joi');
const TransactionType = require('src/domain/enum/TransactionType');

module.exports = () => ({
    post: joi.object().keys({
        type: joi.string().valid(
            TransactionType.DEPOSIT,
            TransactionType.DISCHARGE,
            TransactionType.PAYMENT
        ).required(),
        value: joi.number().required()
    }),
    headers: joi.object().keys({
        authorization: joi.string().required(),
    }),
});

