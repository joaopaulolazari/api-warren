const { Router } = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const compression = require('compression');
const methodOverride = require('method-override');
const health = require('@cloudnative/health-connect');
const jwt = require('express-jwt');
const jsonwebtoken = require('jsonwebtoken');

const InvalidTokenException = require('src/infra/exception/InvalidTokenException');
const ContractException = require('src/infra/exception/ContractException');
const EnumMessage = require('src/domain/enum/EnumMessage');

module.exports = ({
    config,
    routerRegister,
    loggerMiddleware,
    httpErrorMiddleware,
    notFoundMiddleware,
    userRoutes,
    transactionRoutes,
    balanceRoutes,
    swaggerMiddleware
}) => {
    const apiRouter = Router();

    if (config.env !== 'test')
        apiRouter.use(loggerMiddleware);

    let healthcheck = new health.HealthChecker();

    const jwtValidator = jwt({
        secret: 'picadinho',
        algorithms: ['HS256'],
        getToken: (req) => {
            let token = req.headers.authorization;
            if (!token) return new ContractException(EnumMessage.TOKEN_NOT_FOUND);
            if (token.startsWith('Bearer ')) {
                token = token.slice(7, token.length);
            }

            const decoded = jsonwebtoken.verify(token, 'picadinho');
            if (!decoded.user_id) return new ContractException(EnumMessage.TOKEN_NOT_FOUND);

            req.user_id = decoded.user_id;
            return token;
        }
    }).unless({
        path: [
            /^\/api\/docs\/.*/,
            /^\/api\/users\/.*/,
            'api/docs',
            'api/users',
            '/live',
            '/ready',
            '/health',
        ]
    });

    apiRouter
        .use(methodOverride('X-HTTP-Method-Override'))
        .use(cors())
        .use(bodyParser.json())
        .use(compression())
        .use('/api', routerRegister.register(userRoutes))
        .use('/api/docs', swaggerMiddleware)
        .use('/live', health.LivenessEndpoint(healthcheck))
        .use('/ready', health.ReadinessEndpoint(healthcheck))
        .use('/health', health.ReadinessEndpoint(healthcheck))
        .use(jwtValidator)
        .use('/api', routerRegister.register(transactionRoutes))
        .use('/api', routerRegister.register(balanceRoutes))
        .use(function (err, req, res, next) {
            if (err.name === 'JsonWebTokenError') {
                return next(new InvalidTokenException(EnumMessage.TOKEN_INVALID));
            }
            if (err.name === 'UnauthorizedError') {
                return next(new ContractException(EnumMessage.TOKEN_NOT_FOUND));
            }
            if (err) return next(err);
            return next();
        })
        .use(notFoundMiddleware)
        .use(httpErrorMiddleware);

    return apiRouter;
};
