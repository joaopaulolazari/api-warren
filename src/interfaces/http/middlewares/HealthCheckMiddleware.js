module.exports = ({ healthCheck }) => (req, res, next) => {
    return healthCheck.exportMiddleware(req, res, next);
};
