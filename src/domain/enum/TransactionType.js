const Enum = require('src/domain/enum/Enum');

module.exports = Enum({
    DEPOSIT: 'deposit',
    DISCHARGE: 'discharge',
    PAYMENT: 'payment'
});
