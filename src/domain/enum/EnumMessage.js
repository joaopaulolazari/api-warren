const Enum = require('src/domain/enum/Enum');

module.exports = Enum({
    EMPTY_ZONE_INFO: 'Empty zone info',
    COUNTRY_CODE_INVALID_OR_NOT_FOUND: 'country_code invalid or not found.',
    GATEWAY_ERROR: 'Error consulting gateway.',
    USER_NOT_FOUND: 'The User was not found.',
    TRANSACTION_NOT_FOUND: 'The Transaction was not found.',
    INSUFFICIENT_BALANCE: 'Insufficient Balance.',
    TOKEN_NOT_FOUND: 'Token was not found.',
    TOKEN_INVALID: 'Token invalid.',
});
