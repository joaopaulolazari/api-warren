const Enum = require('src/domain/enum/Enum');

module.exports = Enum({
    POST: 'POST',
    GET: 'GET'
});
