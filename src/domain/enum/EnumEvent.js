const Enum = require('src/domain/enum/Enum');

module.exports = Enum({
    ALL: 'all',
    AUTHORIZATION: 'authorization',
    CAPTURE: 'capture',
    CANCELLATION: 'cancellation',
    REVERSAL: 'reversal'
});
