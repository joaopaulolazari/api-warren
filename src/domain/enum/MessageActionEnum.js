const Enum = require('src/domain/enum/Enum');

module.exports = Enum({
    COMPLETE: 'complete',
    DEAD_LETTER: 'deadLetter',
    ABANDON: 'abandon',
    DEFER: 'defer'
});
