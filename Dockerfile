FROM node:14.15.4-alpine

#################
# Fix vulnerabilities in npm lib machine
USER root
WORKDIR /usr/local/lib/node_modules/npm

#Update NPM
RUN npm install -g npm --production

#Remove dependencies (dev)
RUN npm prune --production

#Fix audit
#RUN npm i --package-lock-only
#RUN npm audit fix --registry=https://registry.npmjs.org

#Fix musl package
RUN apk update && apk add musl -f

#Remove this dockerfile commands from applications
ADD https://github.com/Yelp/dumb-init/releases/download/v1.2.2/dumb-init_1.2.2_amd64 /usr/local/bin/dumb-init
#################

RUN addgroup -S nupp && adduser -S -g nupp nupp

ENV HOME=/home/nupp
WORKDIR ${HOME}/app
ENV NODE_PATH=.

COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "${HOME}/app/"]
COPY ["./", "${HOME}/app/"]

#ADD https://github.com/Yelp/dumb-init/releases/download/v1.2.2/dumb-init_1.2.2_amd64 /usr/local/bin/dumb-init

RUN chown -R nupp:nupp ${HOME}/* /usr/local/ && \
    chmod +x /usr/local/bin/dumb-init && \
    npm cache verify && \
    npm install --silent --progress=false --production && \
    chown -R nupp:nupp ${HOME}/*

USER nupp

EXPOSE 3000

CMD ["dumb-init", "npm", "run", "start"]
