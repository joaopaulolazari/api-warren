const { expect, spy } = require('chai');
const CreateUserOperation = require('src/app/operations/user/CreateUserOperation');

describe('App :: Operations :: User :: CreateUserOperation', () => {
    describe('#execute', () => {

        context('when create is successful with user', () => {
            let createUserOperation, userService;
            before(() => {
                userService = {
                    create: () => ({ user_id: 123 })
                };
                createUserOperation = CreateUserOperation({
                    userService,
                    logger: {
                        info: () => {}
                    }
                });
                spy.on(userService, 'create');
            });

            it('returns user', () => {
                const users = createUserOperation.execute('123');
                expect(users).to.be.not.empty();
                expect(userService.create).to.have.been.called.once();
            });
        });

        context('when create is unsuccessful', () => {
            let createUserOperation, userService;
            before(() => {
                userService = {
                    create: () => { throw new Error('any_error'); }
                };
                createUserOperation = CreateUserOperation({
                    userService,
                    logger: {
                        info: () => {}
                    }
                });
                spy.on(userService, 'create');
            });

            it('throw exception to above layer', () => {
                try {
                    createUserOperation.execute('123');
                } catch (error) {
                    expect(error).to.be.instanceof(Error);
                    expect(userService.create).to.have.been.called.once();
                }
            });
        });

    });
});
