const { expect, spy } = require('chai');
const DeleteUserOperation = require('src/app/operations/user/DeleteUserOperation');

describe('App :: Operations :: User :: DeleteUserOperation', () => {
    describe('#execute', () => {

        context('when delete is successful with user', () => {
            let deleteUserOperation, userService;
            before(() => {
                userService = {
                    delete: () => ({ user_id: 123 })
                };
                deleteUserOperation = DeleteUserOperation({
                    userService,
                    logger: {
                        info: () => {}
                    }
                });
                spy.on(userService, 'delete');
            });

            it('returns user', () => {
                const users = deleteUserOperation.execute('123');
                expect(users).to.be.not.empty();
                expect(userService.delete).to.have.been.called.once();
            });
        });

        context('when delete is unsuccessful', () => {
            let deleteUserOperation, userService;
            before(() => {
                userService = {
                    delete: () => { throw new Error('any_error'); }
                };
                deleteUserOperation = DeleteUserOperation({
                    userService,
                    logger: {
                        info: () => {}
                    }
                });
                spy.on(userService, 'delete');
            });

            it('throw exception to above layer', () => {
                try {
                    deleteUserOperation.execute('123');
                } catch (error) {
                    expect(error).to.be.instanceof(Error);
                    expect(userService.delete).to.have.been.called.once();
                }
            });
        });

    });
});
