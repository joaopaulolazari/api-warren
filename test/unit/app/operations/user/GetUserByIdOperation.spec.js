const { expect, spy } = require('chai');
const GetUserByIdOperation = require('src/app/operations/user/GetUserByIdOperation');

describe('App :: Operations :: User :: GetUserByIdOperation', () => {
    describe('#execute', () => {

        context('when get is successful with user', () => {
            let getUserByIdOperation, userService;
            before(() => {
                userService = {
                    getUserById: () => ({ user_id: 123 })
                };
                getUserByIdOperation = GetUserByIdOperation({
                    userService,
                    logger: {
                        info: () => {}
                    }
                });
                spy.on(userService, 'getUserById');
            });

            it('returns user', () => {
                const users = getUserByIdOperation.execute('123');
                expect(users).to.be.not.empty();
                expect(userService.getUserById).to.have.been.called.once();
            });
        });

        context('when get is successful without user', () => {
            let getUserByIdOperation, userService;
            before(() => {
                userService = {
                    getUserById: () => ({})
                };
                getUserByIdOperation = GetUserByIdOperation({
                    userService,
                    logger: {
                        info: () => {}
                    }
                });
                spy.on(userService, 'getUserById');
            });

            it('returns user', () => {
                const users = getUserByIdOperation.execute('123');
                expect(users).to.be.empty();
                expect(userService.getUserById).to.have.been.called.once();
            });
        });

        context('when get is unsuccessful', () => {
            let getUserByIdOperation, userService;
            before(() => {
                userService = {
                    getUserById: () => { throw new Error('any_error'); }
                };
                getUserByIdOperation = GetUserByIdOperation({
                    userService,
                    logger: {
                        info: () => {}
                    }
                });
                spy.on(userService, 'getUserById');
            });

            it('throw exception to above layer', () => {
                try {
                    getUserByIdOperation.execute('123');
                } catch (error) {
                    expect(error).to.be.instanceof(Error);
                    expect(userService.getUserById).to.have.been.called.once();
                }
            });
        });

    });
});
