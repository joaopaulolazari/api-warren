const { expect, spy } = require('chai');
const UpdateUserOperation = require('src/app/operations/user/UpdateUserOperation');

describe('App :: Operations :: User :: UpdateUserOperation', () => {
    describe('#execute', () => {

        context('when update is successful with user', () => {
            let updateUserOperation, userService;
            before(() => {
                userService = {
                    update: () => ({ user_id: 123 })
                };
                updateUserOperation = UpdateUserOperation({
                    userService,
                    logger: {
                        info: () => {}
                    }
                });
                spy.on(userService, 'update');
            });

            it('returns user', () => {
                const users = updateUserOperation.execute('123');
                expect(users).to.be.not.empty();
                expect(userService.update).to.have.been.called.once();
            });
        });

        context('when update is unsuccessful', () => {
            let updateUserOperation, userService;
            before(() => {
                userService = {
                    update: () => { throw new Error('any_error'); }
                };
                updateUserOperation = UpdateUserOperation({
                    userService,
                    logger: {
                        info: () => {}
                    }
                });
                spy.on(userService, 'update');
            });

            it('throw exception to above layer', () => {
                try {
                    updateUserOperation.execute('123');
                } catch (error) {
                    expect(error).to.be.instanceof(Error);
                    expect(userService.update).to.have.been.called.once();
                }
            });
        });

    });
});
