const { expect, spy } = require('chai');
const GetTransactionsByUserIdOperation = require('src/app/operations/transaction/GetTransactionsByUserIdOperation');

describe('App :: Operations :: Transaction :: GetTransactionsByUserIdOperation', () => {
    describe('#execute', () => {

        context('when get is successful with transaction', () => {
            let getTransactionsByUserIdOperation, transactionService;
            before(() => {
                transactionService = {
                    getTransactionsByUserId: () => ({ transaction_id: 123 })
                };
                getTransactionsByUserIdOperation = GetTransactionsByUserIdOperation({
                    transactionService,
                    logger: {
                        info: () => {}
                    }
                });
                spy.on(transactionService, 'getTransactionsByUserId');
            });

            it('returns transaction', async () => {
                const transactions = await getTransactionsByUserIdOperation.execute('123');
                expect(transactions).to.be.not.empty();
                expect(transactionService.getTransactionsByUserId).to.have.been.called.once();
            });
        });

        context('when get is successful without transaction', () => {
            let getTransactionsByUserIdOperation, transactionService;
            before(() => {
                transactionService = {
                    getTransactionsByUserId: () => ({})
                };
                getTransactionsByUserIdOperation = GetTransactionsByUserIdOperation({
                    transactionService,
                    logger: {
                        info: () => {}
                    }
                });
                spy.on(transactionService, 'getTransactionsByUserId');
            });

            it('returns transaction', () => {
                const transactions = getTransactionsByUserIdOperation.execute('123');
                expect(transactions).to.be.empty();
                expect(transactionService.getTransactionsByUserId).to.have.been.called.once();
            });
        });

        context('when get is unsuccessful', () => {
            let getTransactionsByUserIdOperation, transactionService;
            before(() => {
                transactionService = {
                    getTransactionsByUserId: () => { throw new Error('any_error'); }
                };
                getTransactionsByUserIdOperation = GetTransactionsByUserIdOperation({
                    transactionService,
                    logger: {
                        info: () => {}
                    }
                });
                spy.on(transactionService, 'getTransactionsByUserId');
            });

            it('throw exception to above layer', () => {
                try {
                    getTransactionsByUserIdOperation.execute('123');
                } catch (error) {
                    expect(error).to.be.instanceof(Error);
                    expect(transactionService.getTransactionsByUserId).to.have.been.called.once();
                }
            });
        });

    });
});
