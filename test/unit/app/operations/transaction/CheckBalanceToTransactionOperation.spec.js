const { expect, spy } = require('chai');
const CheckBalanceToTransactionOperation = require('src/app/operations/transaction/CheckBalanceToTransactionOperation');

describe('App :: Operations :: Transaction :: CheckBalanceToTransactionOperation', () => {
    describe('#execute', () => {

        context('when get is successful with transaction', () => {
            let checkBalanceToTransactionOperation, transactionService;
            before(() => {
                transactionService = {
                    getBalanceByUserId: () => ({ transaction_id: 123 })
                };
                checkBalanceToTransactionOperation = CheckBalanceToTransactionOperation({
                    transactionService,
                    logger: {
                        info: () => {}
                    }
                });
                spy.on(transactionService, 'getBalanceByUserId');
            });

            it('returns transaction', async () => {
                const transactions = await checkBalanceToTransactionOperation.execute('123');
                expect(transactions).to.be.not.empty();
                expect(transactionService.getBalanceByUserId).to.have.been.called.once();
            });
        });

        context('when get is successful without transaction', () => {
            let checkBalanceToTransactionOperation, transactionService;
            before(() => {
                transactionService = {
                    getBalanceByUserId: () => ({})
                };
                checkBalanceToTransactionOperation = CheckBalanceToTransactionOperation({
                    transactionService,
                    logger: {
                        info: () => {}
                    }
                });
                spy.on(transactionService, 'getBalanceByUserId');
            });

            it('returns transaction', () => {
                const transactions = checkBalanceToTransactionOperation.execute('123');
                expect(transactions).to.be.empty();
                expect(transactionService.getBalanceByUserId).to.have.been.called.once();
            });
        });

        context('when get is unsuccessful', () => {
            let checkBalanceToTransactionOperation, transactionService;
            before(() => {
                transactionService = {
                    getBalanceByUserId: () => { throw new Error('any_error'); }
                };
                checkBalanceToTransactionOperation = CheckBalanceToTransactionOperation({
                    transactionService,
                    logger: {
                        info: () => {}
                    }
                });
                spy.on(transactionService, 'getBalanceByUserId');
            });

            it('throw exception to above layer', () => {
                try {
                    checkBalanceToTransactionOperation.execute('123');
                } catch (error) {
                    expect(error).to.be.instanceof(Error);
                    expect(transactionService.getBalanceByUserId).to.have.been.called.once();
                }
            });
        });

    });
});
