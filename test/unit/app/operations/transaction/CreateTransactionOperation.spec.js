const { expect, spy } = require('chai');
const CreateTransactionOperation = require('src/app/operations/transaction/CreateTransactionOperation');

describe('App :: Operations :: Transaction :: CreateTransactionOperation', () => {
    describe('#execute', () => {

        context('when create is successful with transaction', () => {
            let createTransactionOperation, transactionService, userService, checkBalanceToTransactionOperation;
            before(() => {
                transactionService = {
                    create: () => ({ transaction_id: 123 })
                };
                userService = {
                    getUserById: () => ({ user_id: 123 })
                };
                checkBalanceToTransactionOperation = {
                    execute: () => ({ balance: 23 })
                };
                createTransactionOperation = CreateTransactionOperation({
                    transactionService,
                    userService,
                    checkBalanceToTransactionOperation,
                    logger: {
                        info: () => {}
                    }
                });
                spy.on(transactionService, 'create');
                spy.on(userService, 'getUserById');
                spy.on(checkBalanceToTransactionOperation, 'execute');
            });

            it('returns transaction', async () => {
                const transactions = await createTransactionOperation.execute('123');
                expect(transactions).to.be.not.empty();
                expect(transactionService.create).to.have.been.called.once();
            });
        });

        context('when create is unsuccessful', () => {
            let createTransactionOperation, transactionService, userService, checkBalanceToTransactionOperation;
            before(() => {
                transactionService = {
                    create: () => { throw new Error('any_error'); }
                };
                userService = {
                    getUserById: () => ({ user_id: 123 })
                };
                checkBalanceToTransactionOperation = {
                    execute: () => ({ balance: 23 })
                };
                createTransactionOperation = CreateTransactionOperation({
                    transactionService,
                    userService,
                    checkBalanceToTransactionOperation,
                    logger: {
                        info: () => {}
                    }
                });
                spy.on(transactionService, 'create');
                spy.on(userService, 'getUserById');
                spy.on(checkBalanceToTransactionOperation, 'execute');
            });

            it('throw exception to above layer', () => {
                try {
                    createTransactionOperation.execute('123');
                } catch (error) {
                    expect(error).to.be.instanceof(Error);
                    expect(transactionService.create).to.have.been.called.once();
                }
            });
        });

    });
});
