const { expect, spy } = require('chai');
const GetBalanceByUserOperation = require('src/app/operations/transaction/GetBalanceByUserOperation');

describe('App :: Operations :: Transaction :: GetBalanceByUserOperation', () => {
    describe('#execute', () => {

        context('when get is successful with transaction', () => {
            let getBalanceByUserOperation, transactionService;
            before(() => {
                transactionService = {
                    getBalanceByUserId: () => ({ transaction_id: 123 })
                };
                getBalanceByUserOperation = GetBalanceByUserOperation({
                    transactionService,
                    logger: {
                        info: () => {}
                    }
                });
                spy.on(transactionService, 'getBalanceByUserId');
            });

            it('returns transaction', async () => {
                const transactions = await getBalanceByUserOperation.execute('123');
                expect(transactions).to.be.not.empty();
                expect(transactionService.getBalanceByUserId).to.have.been.called.once();
            });
        });

        context('when get is successful without transaction', () => {
            let getBalanceByUserOperation, transactionService;
            before(() => {
                transactionService = {
                    getBalanceByUserId: () => ({})
                };
                getBalanceByUserOperation = GetBalanceByUserOperation({
                    transactionService,
                    logger: {
                        info: () => {}
                    }
                });
                spy.on(transactionService, 'getBalanceByUserId');
            });

            it('returns transaction', () => {
                const transactions = getBalanceByUserOperation.execute('123');
                expect(transactions).to.be.empty();
                expect(transactionService.getBalanceByUserId).to.have.been.called.once();
            });
        });

        context('when get is unsuccessful', () => {
            let getBalanceByUserOperation, transactionService;
            before(() => {
                transactionService = {
                    getBalanceByUserId: () => { throw new Error('any_error'); }
                };
                getBalanceByUserOperation = GetBalanceByUserOperation({
                    transactionService,
                    logger: {
                        info: () => {}
                    }
                });
                spy.on(transactionService, 'getBalanceByUserId');
            });

            it('throw exception to above layer', () => {
                try {
                    getBalanceByUserOperation.execute('123');
                } catch (error) {
                    expect(error).to.be.instanceof(Error);
                    expect(transactionService.getBalanceByUserId).to.have.been.called.once();
                }
            });
        });

    });
});
