const { expect, spy } = require('chai');
const UserModel = require('src/infra/database/mongo/models/UserModel');
const { generateModel } = require('test/support/dataFaker/models/UserModelDataFaker');
const mongoose = require('mongoose');

describe('Infra :: Database :: Mongo :: Models :: UserModel', () => {

    const config =  {
        db: {
            collections: {
                user: {
                    name: 'user',
                    version: '1.0.0',
                    isGlobal: true
                }
            }
        }
    };

    afterEach(function () {
        mongoose.models = {};
    });

    context('Initializes mongoose schema', () => {
        let providerConnection;

        before(() => {
            providerConnection = {
                connection: {
                    models: () => null,
                    model: (name, schema) => mongoose.model(name, schema),
                }
            };
            spy.on(providerConnection.connection, 'model');
        });

        it('returns model name and schema', () => {
            const userModel = new UserModel({
                providerConnection,
                config
            }).model;

            expect(userModel).to.be.exist();
            expect(providerConnection.connection.model).to.be.called.once();
        });
    });

    context('when fields is ok', () => {
        let userModel, data;
        before(() => {
            const MockProviderConnection = {
                connection: {
                    models: () => null,
                    model: (name, schema) => mongoose.model(name, schema),
                }
            };

            userModel = new UserModel({
                providerConnection: MockProviderConnection,
                config
            }).model;
            data = generateModel(1)[0];
        });

        it('returns validate success', () => {
            const model = userModel(data);
            const error =  model.validateSync(data);

            expect(error).to.be.undefined();
        });
    });

    context('when type fields is wrong', () => {
        let userModel, data, expectedErrors;
        before(() => {
            const MockProviderConnection = {
                connection: {
                    models: () => null,
                    model: (name, schema) => mongoose.model(name, schema),
                }
            };
            config.db.collections.user.isGlobal = false;

            userModel = new UserModel({
                providerConnection: MockProviderConnection,
                config
            }).model;
            data = generateModel(1)[0];
            Object.keys(data).forEach(item => {
                data[item] = [];
            });
            expectedErrors = [
                'user_id',
                'name'
            ];
        });

        it('returns errors fields', () => {
            const model = userModel(data);
            const error =  model.validateSync(data);

            expect(error.errors).to.have.all.keys(expectedErrors);
        });
    });

    context('when required fields not present', () => {
        let userModel, data;
        before(() => {
            const MockProviderConnection = {
                connection: {
                    models: () => null,
                    model: (name, schema) => mongoose.model(name, schema),
                }
            };
            config.db.collections.user.isGlobal = true;

            userModel = new UserModel({
                providerConnection: MockProviderConnection,
                config
            }).model;
            data = generateModel(1)[0];
            delete data.user_id;
        });

        it('returns model name and schema', () => {
            const model = userModel(data);
            const error =  model.validateSync(data);

            expect(error.errors).to.have.all.keys('user_id');
        });
    });
});
