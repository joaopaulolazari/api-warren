const { expect, spy } = require('chai');
const TransactionModel = require('src/infra/database/mongo/models/TransactionModel');
const { generateModel } = require('test/support/dataFaker/models/TransactionModelDataFaker');
const mongoose = require('mongoose');

describe('Infra :: Database :: Mongo :: Models :: TransactionModel', () => {

    const config =  {
        db: {
            collections: {
                transaction: {
                    name: 'transaction',
                    version: '1.0.0',
                    isGlobal: true
                }
            }
        }
    };

    afterEach(function () {
        mongoose.models = {};
    });

    context('Initializes mongoose schema', () => {
        let providerConnection;

        before(() => {
            providerConnection = {
                connection: {
                    models: () => null,
                    model: (name, schema) => mongoose.model(name, schema),
                }
            };
            spy.on(providerConnection.connection, 'model');
        });

        it('returns model name and schema', () => {
            const transactionModel = new TransactionModel({
                providerConnection,
                config
            }).model;

            expect(transactionModel).to.be.exist();
            expect(providerConnection.connection.model).to.be.called.once();
        });
    });

    context('when fields is ok', () => {
        let transactionModel, data;
        before(() => {
            const MockProviderConnection = {
                connection: {
                    models: () => null,
                    model: (name, schema) => mongoose.model(name, schema),
                }
            };

            transactionModel = new TransactionModel({
                providerConnection: MockProviderConnection,
                config
            }).model;
            data = generateModel(1)[0];
        });

        it('returns validate success', () => {
            const model = transactionModel(data);
            const error =  model.validateSync(data);

            expect(error).to.be.undefined();
        });
    });

    context('when type fields is wrong', () => {
        let transactionModel, data, expectedErrors;
        before(() => {
            const MockProviderConnection = {
                connection: {
                    models: () => null,
                    model: (name, schema) => mongoose.model(name, schema),
                }
            };
            config.db.collections.transaction.isGlobal = false;

            transactionModel = new TransactionModel({
                providerConnection: MockProviderConnection,
                config
            }).model;
            data = generateModel(1)[0];
            Object.keys(data).forEach(item => {
                data[item] = [];
            });
            expectedErrors = [
                'transaction_id',
                'user_id',
                'type',
                'value',
            ];
        });

        it('returns errors fields', () => {
            const model = transactionModel(data);
            const error =  model.validateSync(data);

            expect(error.errors).to.have.all.keys(expectedErrors);
        });
    });

    context('when required fields not present', () => {
        let transactionModel, data;
        before(() => {
            const MockProviderConnection = {
                connection: {
                    models: () => null,
                    model: (name, schema) => mongoose.model(name, schema),
                }
            };
            config.db.collections.transaction.isGlobal = true;

            transactionModel = new TransactionModel({
                providerConnection: MockProviderConnection,
                config
            }).model;
            data = generateModel(1)[0];
            delete data.transaction_id;
        });

        it('returns model name and schema', () => {
            const model = transactionModel(data);
            const error =  model.validateSync(data);

            expect(error.errors).to.have.all.keys('transaction_id');
        });
    });
});
