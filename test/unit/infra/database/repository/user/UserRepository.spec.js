const { expect, spy } = require('chai');
const UserRepository = require('src/infra/database/repository/user/UserRepository');

describe('Infra :: Database :: Repository :: User :: UserRepository', () => {

    describe('#getUser', () => {

        context('when find user', () => {

            let userRepository, userModel;
            before(() => {
                userModel = {
                    getSchema: () => null,
                    model: {}
                };

                userRepository = new UserRepository({ userModel });
                userRepository.getUser = () => ({ users: 'any_data' });

                spy.on(userRepository, 'getUser');
            });

            it('returns one user', async () => {
                const user = await userRepository.getUser('userId');
                expect(userRepository.getUser).to.have.been.called.once();
                expect(user).not.be.empty();
            });
        });

        context('when not find user', () => {
            let userRepository, userModel;
            before(() => {
                userModel = {
                    getSchema: () => null,
                    model: {}
                };
                userRepository = new UserRepository({ userModel });
                userRepository.getUser = () => Promise.resolve(null);

                spy.on(userRepository, 'getUser');
            });

            it('throw not found exception', async () => {
                await userRepository.getUser('userId').catch(error => {
                    expect(userRepository.getUser).to.have.been.called.once();
                    expect(error.message).to.be.equal('The User was not found.');
                    expect(error).to.be.instanceof(Error);
                });
            });
        });
    });

    describe('#updateUser', () => {
        context('when data created with success', () => {
            let userRepository, userModel;
            before(() => {
                userModel = {
                    getSchema: () => null,
                    model: {}
                };

                userRepository = new UserRepository({ userModel });
                userRepository.updateUser = (userId, user) => ({ user_id: userId, ...user });

                spy.on(userRepository, 'updateUser');
            });

            it('returns persisted data', async () => {
                const userId = '123';
                const data = { data_id: 'test' };
                const persisted = await userRepository.updateUser(userId, data);

                expect(persisted).to.be.exist();
                expect(userRepository.updateUser).to.have.been.called.once();
                expect(userRepository.updateUser).to.have.been.called.with(userId, data).once();
            });
        });
    });

    describe('#deleteUser', () => {

        context('when delete user', () => {

            let userRepository, userModel;
            before(() => {
                userModel = {
                    getSchema: () => null,
                    isGlobal: true,
                    collectionVersion: '1.0.0'
                };

                userRepository = new UserRepository({ userModel });
                userRepository.deleteUser = () => (true);

                spy.on(userRepository, 'deleteUser');
            });

            it('returns true', async () => {
                const user = await userRepository.deleteUser('userId');
                expect(userRepository.deleteUser).to.have.been.called.once();
                expect(user).to.be.equals(true);
            });
        });
    });
});
