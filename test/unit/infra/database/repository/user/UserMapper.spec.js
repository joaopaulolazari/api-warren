const { expect } = require('chai');
const UserMapper = require('src/infra/database/repository/user/UserMapper');

describe('#toEntity', () => {

    context('when discart some fields', () => {

        let user;
        beforeEach(() => {
            user = {
                user_id: 'any_data'
            };
        });

        it('returns entity with mappead fields', () => {
            const entity = UserMapper.toEntity(user);

            expect(entity).to.haveOwnProperty('user_id');
        });

        it ('returns immutable values', () => {
            const entity = UserMapper.toEntity(user);

            Object.keys(entity).every(key =>
                expect(entity[key] === 'any_data')
            );
        });

    });
    context('when fields are missing', () => {
        let user;
        beforeEach(() => {
            user = {
                user_id: 'any_data'
            };
        });

        it('returns missing fields as undefined', () => {
            const entity = UserMapper.toEntity(user);
            expect(entity).to.haveOwnProperty('user_id');
        });
    });
});

describe('#toDatabase', () => {

    context('when fields are missing', () => {
        it('returns all fields that received by param', () => {

            const toDatabase = UserMapper.toDatabase({
                user_id: 'any_data'
            });

            Object.keys(toDatabase).every(key =>
                expect(toDatabase[key] === 'any_data')
            );
        });
    });
});
