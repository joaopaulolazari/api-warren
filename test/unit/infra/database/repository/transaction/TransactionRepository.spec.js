const { expect, spy } = require('chai');
const TransactionRepository = require('src/infra/database/repository/transaction/TransactionRepository');

describe('Infra :: Database :: Repository :: Transaction :: TransactionRepository', () => {

    describe('#getByUserId', () => {

        context('when find transaction', () => {

            let transactionRepository, transactionModel;
            before(() => {
                transactionModel = {
                    getSchema: () => null,
                    model: {}
                };

                transactionRepository = new TransactionRepository({ transactionModel });
                transactionRepository.getByUserId = () => ({ transactions: 'any_data' });

                spy.on(transactionRepository, 'getByUserId');
            });

            it('returns one transaction', async () => {
                const transaction = await transactionRepository.getByUserId('transactionId');
                expect(transactionRepository.getByUserId).to.have.been.called.once();
                expect(transaction).not.be.empty();
            });
        });

        context('when not find transaction', () => {
            let transactionRepository, transactionModel;
            before(() => {
                transactionModel = {
                    getSchema: () => null,
                    model: {}
                };
                transactionRepository = new TransactionRepository({ transactionModel });
                transactionRepository.getByUserId = () => Promise.resolve(null);

                spy.on(transactionRepository, 'getByUserId');
            });

            it('throw not found exception', async () => {
                await transactionRepository.getByUserId('transactionId').catch(error => {
                    expect(transactionRepository.getByUserId).to.have.been.called.once();
                    expect(error.message).to.be.equal('The transaction was not found.');
                    expect(error).to.be.instanceof(Error);
                });
            });
        });
    });

    describe('#getTransaction', () => {

        context('when find transaction', () => {

            let transactionRepository, transactionModel;
            before(() => {
                transactionModel = {
                    getSchema: () => null,
                    model: {}
                };

                transactionRepository = new TransactionRepository({ transactionModel });
                transactionRepository.getTransaction = () => ({ transactions: 'any_data' });

                spy.on(transactionRepository, 'getTransaction');
            });

            it('returns one transaction', async () => {
                const transaction = await transactionRepository.getTransaction('transactionId');
                expect(transactionRepository.getTransaction).to.have.been.called.once();
                expect(transaction).not.be.empty();
            });
        });

        context('when not find transaction', () => {
            let transactionRepository, transactionModel;
            before(() => {
                transactionModel = {
                    getSchema: () => null,
                    model: {}
                };
                transactionRepository = new TransactionRepository({ transactionModel });
                transactionRepository.getTransaction = () => Promise.resolve(null);

                spy.on(transactionRepository, 'getTransaction');
            });

            it('throw not found exception', async () => {
                await transactionRepository.getTransaction('transactionId').catch(error => {
                    expect(transactionRepository.getTransaction).to.have.been.called.once();
                    expect(error.message).to.be.equal('The transaction was not found.');
                    expect(error).to.be.instanceof(Error);
                });
            });
        });
    });

    describe('#updateTransaction', () => {
        context('when data created with success', () => {
            let transactionRepository, transactionModel;
            before(() => {
                transactionModel = {
                    getSchema: () => null,
                    model: {}
                };

                transactionRepository = new TransactionRepository({ transactionModel });
                transactionRepository.updateTransaction = (transactionId, transaction) => ({ transaction_id: transactionId, ...transaction });

                spy.on(transactionRepository, 'updateTransaction');
            });

            it('returns persisted data', async () => {
                const transactionId = '123';
                const data = { data_id: 'test' };
                const persisted = await transactionRepository.updateTransaction(transactionId, data);

                expect(persisted).to.be.exist();
                expect(transactionRepository.updateTransaction).to.have.been.called.once();
                expect(transactionRepository.updateTransaction).to.have.been.called.with(transactionId, data).once();
            });
        });
    });

    describe('#deleteTransaction', () => {

        context('when delete transaction', () => {

            let transactionRepository, transactionModel;
            before(() => {
                transactionModel = {
                    getSchema: () => null,
                    isGlobal: true,
                    collectionVersion: '1.0.0'
                };

                transactionRepository = new TransactionRepository({ transactionModel });
                transactionRepository.deleteTransaction = () => (true);

                spy.on(transactionRepository, 'deleteTransaction');
            });

            it('returns true', async () => {
                const transaction = await transactionRepository.deleteTransaction('transactionId');
                expect(transactionRepository.deleteTransaction).to.have.been.called.once();
                expect(transaction).to.be.equals(true);
            });
        });
    });
});
