const { expect } = require('chai');
const TransactionMapper = require('src/infra/database/repository/transaction/TransactionMapper');

describe('#toEntity', () => {

    context('when discart some fields', () => {

        let transaction;
        beforeEach(() => {
            transaction = {
                transaction_id: 'any_data'
            };
        });

        it('returns entity with mappead fields', () => {
            const entity = TransactionMapper.toEntity(transaction);

            expect(entity).to.haveOwnProperty('transaction_id');
        });

        it ('returns immutable values', () => {
            const entity = TransactionMapper.toEntity(transaction);

            Object.keys(entity).every(key =>
                expect(entity[key] === 'any_data')
            );
        });

    });
    context('when fields are missing', () => {
        let transaction;
        beforeEach(() => {
            transaction = {
                transaction_id: 'any_data'
            };
        });

        it('returns missing fields as undefined', () => {
            const entity = TransactionMapper.toEntity(transaction);
            expect(entity).to.haveOwnProperty('transaction_id');
        });
    });
});

describe('#toDatabase', () => {

    context('when fields are missing', () => {
        it('returns all fields that received by param', () => {

            const toDatabase = TransactionMapper.toDatabase({
                transaction_id: 'any_data'
            });

            Object.keys(toDatabase).every(key =>
                expect(toDatabase[key] === 'any_data')
            );
        });
    });
});
