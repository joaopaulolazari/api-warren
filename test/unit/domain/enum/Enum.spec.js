const { expect } = require('chai');
const Enum = require('src/domain/enum/Enum');

describe('Domain :: Enum :: Enum', () => {

    context('#values() method', () => {

        it('should return the enum values', () => {
            const myEnum = Enum({
                SOME_VALUE: 'someValue',
                ANOTHER_VALUE: 'anotherValue'
            });

            const enumValues = myEnum.values();

            expect(enumValues.length).to.equal(2);
            expect(enumValues).to.members(['someValue', 'anotherValue']);
        });
    });

    context('#keys() method', () => {

        it('should return the enum keys', () => {
            const myEnum = Enum({
                SOME_VALUE: 'someValue',
                ANOTHER_VALUE: 'anotherValue'
            });

            const enumValues = myEnum.keys();

            expect(enumValues.length).to.equal(2);
            expect(enumValues).to.members(['SOME_VALUE', 'ANOTHER_VALUE']);
        });
    });
});