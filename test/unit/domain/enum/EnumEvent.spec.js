const { expect } = require('chai');
const EnumEvent = require('src/domain/enum/EnumEvent');

describe('Domain :: Enum :: EnumEvent', () => {

    context('#values() method', () => {
        it('should return the enum values', () => {
            const values = EnumEvent.values();

            expect(values).to.have.members(['all', 'authorization', 'capture', 'cancellation', 'reversal']);
        });
    });

    context('#keys() method', () => {

        it('should return the enum keys', () => {
            const keys = EnumEvent.keys();

            expect(keys).to.have.members(['ALL', 'AUTHORIZATION', 'CAPTURE', 'CANCELLATION', 'REVERSAL']);
        });

    });
});
