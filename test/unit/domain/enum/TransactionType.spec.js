const { expect } = require('chai');
const TransactionType = require('src/domain/enum/TransactionType');

describe('Domain :: Enum :: TransactionType', () => {

    context('#values() method', () => {
        it('should return the enum values', () => {
            const values = TransactionType.values();

            expect(values).to.have.members(['deposit', 'discharge', 'payment']);
        });
    });

    context('#keys() method', () => {

        it('should return the enum keys', () => {
            const keys = TransactionType.keys();

            expect(keys).to.have.members(['DEPOSIT', 'DISCHARGE', 'PAYMENT']);
        });

    });
});
