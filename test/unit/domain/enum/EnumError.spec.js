const { expect } = require('chai');
const { applicationError, applicationErrorCode } = require('src/domain/enum/EnumError');

describe('Domain :: Enum :: EnumError', () => {

    describe('#applicationError', () => {

        context('#values() method', () => {
            it('should return the enum values', () => {
                const values = applicationError.values();
    
                expect(values).to.have.members(['business', 'notFound', 'contract', 'integration', 'operation']);
            });
        });
    
        context('#keys() method', () => {
            it('should return the enum keys', () => {
                const keys = applicationError.keys();
    
                expect(keys).to.have.members(['BUSINESS', 'NOT_FOUND', 'CONTRACT', 'INTEGRATION', 'OPERATION']);
            });
        });

    });

    describe('#applicationErrorCode', () => {

        context('#values() method', () => {
            it('should return the enum values', () => {
                const values = applicationErrorCode.values();
    
                expect(values).to.have.members(['business', 'notFound', 'contract', 'integration', 'operation', 'business']);
            });
        });
    
        context('#keys() method', () => {
            it('should return the enum keys', () => {
                const keys = applicationErrorCode.keys();
    
                expect(keys).to.have.members(['422', '404', '400', '504', '500', '116']);
            });
        });

    });
    
});
