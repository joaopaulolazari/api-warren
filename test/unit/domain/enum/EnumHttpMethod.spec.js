const { expect } = require('chai');
const EnumHttpMethod = require('src/domain/enum/EnumHttpMethod');

describe('Domain :: Enum :: EnumHttpMethod', () => {

    context('#values() method', () => {
        it('should return the enum values', () => {
            const values = EnumHttpMethod.values();

            expect(values).to.have.members(['POST', 'GET']);
        });
    });

    context('#keys() method', () => {

        it('should return the enum keys', () => {
            const keys = EnumHttpMethod.keys();

            expect(keys).to.have.members(['POST', 'GET']);
        });

    });
});
