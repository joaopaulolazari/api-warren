const dataFaker = require('test/support/dataFaker/dataFaker');
const { newUUID } = require('src/infra/support/identifierGenerator');

const generateModel = quantity => {
    const objs = [];
    for (let index = 0; index < quantity; index++) {
        objs.push(createUser());
    }
    return objs;
};

const createUser = () => ({
    user_id: newUUID(),
    name: dataFaker.string()
});

module.exports = { generateModel };
