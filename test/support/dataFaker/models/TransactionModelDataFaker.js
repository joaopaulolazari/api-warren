const dataFaker = require('test/support/dataFaker/dataFaker');
const { newUUID } = require('src/infra/support/identifierGenerator');

const generateModel = quantity => {
    const objs = [];
    for (let index = 0; index < quantity; index++) {
        objs.push(createTransaction());
    }
    return objs;
};

const createTransaction = () => ({
    transaction_id: newUUID(),
    user_id: newUUID(),
    type: ['deposit', 'discharge', 'payment'][dataFaker.integer({ min: 0, max: 2 })],
    value: dataFaker.integer()
});

module.exports = { generateModel };
