module.exports = {

    compareMsgs(arrMsgsExpected, arrReceived, ...args) {
        const prop = args.find(el => typeof el === 'string') || 'message';
        const bool = args.find(el => typeof el === 'boolean');
        const compareEqual = typeof bool !== 'undefined' ? bool : true;

        // const missingFields = arrReceived.filter(obj => !arrMsgsExpected.includes(prop === 'message' ? obj[prop].replace(/"/g, '') : obj[prop]));
        // if(missingFields.length > 0) {
        //     console.log(missingFields);
        // }
        if (arrMsgsExpected.length !== arrReceived.length) return false;

        return arrReceived.every(obj => {
            const valReceived = prop === 'message' ? obj[prop].replace(/"/g, '') : obj[prop];
            if (typeof obj !== 'object') return arrMsgsExpected.some(valExpected => obj === valExpected);
            if (!compareEqual) return arrMsgsExpected.some(valExpected => valReceived.includes(valExpected));
            
            return arrMsgsExpected.some(valExpected => valReceived === valExpected);
        });
    },

    makeLegalDocument() {
        const numbers = '0123456789';
        const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        const threeOrFour = ~~(Math.random() * (5 - 3)) + 3;
        let res = '';
        for (let i = 0; i < (threeOrFour + 9); i++) {
            if (i < threeOrFour) res += chars.charAt(~~(Math.random() * chars.length));
            else if (i < (threeOrFour + 6)) res += numbers.charAt(~~(Math.random() * numbers.length));
            else res += `${chars}${numbers}`.charAt(~~(Math.random() * `${chars}${numbers}`.length));
        }
        return res;
    },
    
    uuidv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
            let r = Math.random() * 16 | 0,
                v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    },

    isGuid(stringToTest) {
        if (stringToTest[0] === '{') {
            stringToTest = stringToTest.substring(1, stringToTest.length - 1);
        }
        // eslint-disable-next-line no-useless-escape
        const regexGuid = /^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$/gi;
        return regexGuid.test(stringToTest);
        
    },
};