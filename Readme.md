# API Warren
Este projeto consiste em um microserviço que compreende todo o backend do projeto.
Utilizo para a criação desse serviço uma Api Boilerplate que eu mesmo mantenho para acelerar os processos de api`s para projetos pessoais e etc.

Neste projeto eu utilizo as seguintes tecnologias:

* [NodeJs](https://nodejs.org/en/) (v14.15.4)
* [MongoDB](https://www.mongodb.com/)
* [Docker](https://www.docker.com/)
* [Swagger](https://swagger.io/)

Também utilizo a metodologia chamada de Domain-Driven Design (DDD) pois entendo ser, para mim, uma metodologia que faz mais sentido quando se diz respeito a arquitetura de microserviços.
Modelo de criação da chave JWT, via [https://jwt.io/](https://jwt.io/):

```
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiODA2ZjI3NDAtOGQ4My00MDQzLWFhNTUtYTc2NjA5NDZmODE0In0.QFBUtHEFSyzC9abiiEjueaReGWeo_b7qCmKP2QUfaGY
```

Trago também um conceito de usar o userId dentro de uma chave de autenticação JWT por entender que não é uma medida de segurança expor tal informação, então imbuti essa informação dentro da chave de autenticação para utilização nas consultas e transações.

Deixei também exposta as rotas de Users, somente para utilizar a criação de novos usuários para testes.

### Desenvolvimento
```sh
docker-compose -f "docker-compose.environment.yml" up -d --build
npm i
npm run dev
```

### Documentação API
Nesse projeto usamos o Swagger para manter uma documentação da API e também facilitar os testes de API para os QAs ou qualquer outro interessado.
Um ponto interessante dessa utilização é que mantenho um código de auto-geração do swagger, não necessitando criar sempre a documentação de cada rota, já provendo um valor mínimo de documentação para a rota criada.
Depois de subir a aplicação de desenvolvimento podemos acessar a documentação via [http://localhost:3000/api/docs/](http://localhost:3000/api/docs/)

### Testes
Para rodar os testes é só executar o seguinte código.
```sh
npm run test
```

### Melhorias e críticas ao desenvolvimento
Algumas coisas poderiam serem melhoradas:

* Talvez usar a minha api-boilerplate traga um overload de libs que não estão sendo utilizadas e talvez poderiamos tirar as libs para enxutar o sistema.
* Teria que aumentar a cobertura de código, já que fiz testes para mostrar minha proeficiencia com testes mas nao cheguei a um coverage de 100%
* Implementar testes de features para garantir contratos.
* Melhorar os retornos e tratamento de erros.
