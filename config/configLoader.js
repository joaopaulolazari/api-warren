require('dotenv').load();

const syswidecas = require('syswide-cas');

const path = require('path');
const ENV = process.env.NODE_ENV;
const INSTANCE = process.env.NODE_INSTANCE;

const info = require(path.join(__dirname, 'info.json'));
const baseConfig = {
    env: ENV,
    instance: INSTANCE,
    info: info
};

function updateEnvAndInstance(obj) {
    for (var key in obj) {
        if (typeof obj[key] === 'object') {
            updateEnvAndInstance(obj[key]);
        } else {
            if(typeof obj[key] === 'string' || obj[key] instanceof String){
                obj[key] = obj[key].replace(/\$env/g, ENV);
                obj[key] = obj[key].replace(/\$instance/g, INSTANCE);
            }
        }
    }
}

module.exports = {
    loadEnvironment: () => {

        var localEnv = '';

        if(ENV === 'LOCAL'){
            localEnv = require('scripts/properties/local');
            console.log('Overriding with LOCAL config');
        } else if (ENV === 'DOCKER'){
            localEnv = require('scripts/properties/docker');
            console.log('Overriding with DOCKER config');
        } else {
            localEnv = require('./env');
            updateEnvAndInstance(localEnv);
        }

        return {
            ...baseConfig,
            ...localEnv
        };
    },

    loadCerts: () => {
        syswidecas.addCAs('config/ssl');
    }
};
